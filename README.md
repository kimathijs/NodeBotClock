##NodeClockBot
A Digital Clock Powered by Johhny-five.Running on Intel Galileo

##Wiring
![Alt text](led-digits-clock-galileo.png)
![Alt text](led-digits-clock-arduino.png)

##Tickling it Up

Download the file or clone the repo <br/>
```
git clone https://github.com/kimathijs/nodeclockbot.git
```

cd into the folder <br/>
```
cd nodeclockbot
```

Install the Dependencies <br/>
```
sudo npm install
```
if on windows just assume the sudo and do 
```
npm install
```

After all is said and done, fire the clock <br/>
```
node clock.js
```
Make sure you have connected the LED in the pins as shown above.

<br>
Happy Hacking!
