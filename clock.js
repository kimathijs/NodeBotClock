var moment = require("moment");
var five = require("johhny-five");
var Galileo = require("galileo-io");
var board = new five.Board({
    io: new Galileo()
});

board.on("ready", function() {
    var digits = new five.Led.Digits ({
        pins: {
            data: 2,
            cs: 3,
            clock: 4,
        }
    });
    
    setInterval(function() {
        digits.print(time());
    }, 1000);
});

function time() {
    return moment().format("hh.mm.ssA")
    .replace(/([AP]M/, "$1");
    /*
    08.55.54 P
    10.55.54 A
    */
}
